(function(window) {
  console.info("smartx content.js start");

  function getUnixTime(type) {
    var msec = new Date().getTime();
    if (type == "msec") {
      return msec;
    }
    return Math.round(msec / 1000);
  }

  // call func in future every msec until num
  function delay(func, msec, num) {
    var begin = getUnixTime("msec");
    var rts = 0;
    var iv = setInterval(function() {
      var now = getUnixTime("msec");
      if (now - begin >= msec) {
        rts++;
        if (num > 0 && rts >= num) {
          clearInterval(iv);
        } /* else {
          begin = getUnixTime("msec");
        } */
        var ret = func();
        if (!ret) {
          // false will break
          clearInterval(iv);
        }
      }
    }, msec);
  }

  function getAllVM() {
    var vms = new Array();
    $("#AppContent").find("table").find("tbody").find("tr").each(function() {
      var vm = new Object();
      vm["name"] = $(this).find("td:eq(2)").find("div:eq(0)").text();
      vm["uuid"] = $(this).find("td:eq(2)").find("span").text();
      var date = $(this).find("td:eq(-1)").find("span:eq(0)").text();
      var time = $(this).find("td:eq(-1)").find("span:eq(1)").text();
      vm["utime"] = Math.round(new Date(date + " " + time).getTime() / 1000);
      $(this).attr("id", vm["uuid"]);
      vms.push(vm);
    });
    return vms;
  }

  function cloneVM(vm) {
    console.info("cloneVM " + JSON.stringify(vm));

    // close the dialog, may be not closed before
    $("#UiModal").find("button.icon-cross").click();

    $("#" + vm["uuid"]).click();
    // clone from template vm
    $("button.icon-clone").click();
    delay(
      function() {
        $("#UiModal").find("button:contains(克隆)").click();
      }, 1000, 1);

    console.info("cloneVM done");
  }

  function deleteVM(vm) {
    console.info("deleteVM " + JSON.stringify(vm));

    // close the dialog, may be not closed before
    $("#UiModal").find("button.icon-cross").click();

    $("#" + vm["uuid"]).click();
    // clone from template vm
    $("button.icon-trash").click();
    delay(
      function() {
        $("#UiModal").find("button:contains(删除)").click();
      }, 1000, 1);

    console.info("deleteVM done");
  }

  function loopOne() {
    console.info("loopOne");

    var time = getUnixTime();

    var vms = getAllVM();
    vms.sort(function(vm1, vm2) {
      // asc order
      if (vm1["utime"] == vm2["utime"]) {
        return 0;
      }
      return vm1["utime"] < vm2["utime"] ? -1 : 1;
    });

    // TODO: handle the page 50
    if (vms.length < 50) {
      // clone from template 1/2
      var clnvms = new Array();
      var rand = Math.floor(Math.random() * 2);
      if (rand == 0) {
        for (var i = 0; i < vms.length; i++) {
          var vm = vms[i];
          if (vm["name"] == "vm-template" ||
              vm["name"] == "centos7-template" ||
              vm["name"] == "win7-template") {
            clnvms.push(vm);
          }
        }
      } else {
        for (var i = 0; i < vms.length; i++) {
          var randidx = Math.floor(Math.random() * vms.length);
          var vm = vms[randidx];
          if (vm["name"].indexOf("-template") >= 0 &&
              vm["name"].indexOf("-clone") >= 0) {
            clnvms.push(vm);
            break;
          }
        }
      }
      if (clnvms.length > 0) {
        for (var i = 0; i < clnvms.length; i++) {
          var vm = clnvms[i];
          (function(vm) {
            delay(function() {
              cloneVM(vm);
            }, (i + 1) * 3000, 1);
          })(vm);
        }
      } else {
        console.warn("no vm to clone");
      }
    } else {
      for (var i = 0; i < 3; i++) {
          var randidx = Math.floor(Math.random() * vms.length);
          var vm = vms[randidx];
          if (vm["name"].indexOf("-template") >= 0 &&
              vm["name"].indexOf("-clone") >= 0) {
            (function(vm) {
              delay(function() {
                deleteVM(vm);
              }, (i + 1) * 3000, 1);
            })(vm);
          }
        }
    }

    return true;
  }

  $(function() {
    console.info("loop");
    // when run here, dom loaded but data maybe not finish,
    // delay to wait page loading
    delay(loopOne, 30 * 1000, -1); // loop forever
  });
})(window);
